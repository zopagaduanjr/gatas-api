# Milk products API

💾 Backend: https://caring-story-production.up.railway.app/products

## Data from edamama milk catalog

![edamama milk catalog](./mood_board.png)

## Getting Started

Run `node index.js` for a dev server.

Read `package.json` for more information.
