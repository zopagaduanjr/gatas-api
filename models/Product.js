const mongoose = require("mongoose");

const ProductSchema = mongoose.Schema({
  id: Number,
  title: String,
  price: Number,
  description: String,
  category: String,
  image: String,
  __v: { type: Number, select: false },
});

module.exports = mongoose.model("Product", ProductSchema);
