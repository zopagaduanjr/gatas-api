require("dotenv/config");

const bodyParser = require("body-parser");
const cors = require("cors");
const express = require("express");
const mongoose = require("mongoose");

const app = express();

const Product = require("./models/Product");

//Middlewares
app.use(cors());
app.use(bodyParser.json());

app.get("/products", async (req, res) => {
  try {
    const products = await Product.find({});
    res.status(200).json(products);
  } catch (err) {
    res.status(400).json({ message: err });
  }
});

app.post("/products/new", (req, res) => {
  const product = new Product({
    id: req.body.id,
    title: req.body.title,
    price: req.body.price,
    description: req.body.description,
    category: req.body.category,
    image: req.body.image,
  });
  product
    .save()
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => {
      res.status(400).json({ message: req.body });
    });
});

mongoose
  .connect(process.env.DB_CONNECTION)
  .catch((error) => console.error(error));

const port = process.env.PORT || 8024;
app.listen(port);
